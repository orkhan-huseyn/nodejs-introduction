# My REST API

GET /todos - list all todos
GET /todos/:id - return single todo
POST /todos - create new todo { title: string; compeleted: boolean; }
PUT /todos/:id - update todo with given id { title: string; compeleted: boolean; }
DELETE /todos/:id - delete todo with given id
const http = require("http");
const fs = require("fs");

const server = http.createServer(function (req, res) {
  console.log(req.method + " " + req.url);

  let fileUrl = "";

  switch (req.url) {
    case "/":
      fileUrl = "index.html";
      break;
    case "/about":
      fileUrl = "about.html";
      break;
    case "/contact":
      fileUrl = "contact.html";
      break;
    default:
      fileUrl = "404.html";
  }

  fs.readFile(fileUrl, function (err, content) {
    res.end(content.toString());
  });
});

server.listen(8080);

const express = require("express");
const path = require("path");
const app = express();

app.get("/", function (req, res) {
  res.sendFile(path.resolve("index.html"));
});

app.post("/", function (req, res) {
  res.json({
    message: "OK",
  });
});

app.get("/contact", function (req, res) {
  res.sendFile(path.resolve("contact.html"));
});

app.get("/about", function (req, res) {
  res.sendFile(path.resolve("about.html"));
});

app.listen(8080);

const http = require("http");
const express = require("express");
const app = express();

app.use(express.json());
// app.use((req, res, next)=> {
//     if (req.headers["authorization"]) {
//         next();
//     }
//     res.status(401).send();
// });

let lastId = 1,
  todos = [];

app.get("/todos", (req, res) => {
  const page = req.query.page ? Number(req.query.page) : 1;
  const limit = req.query.limit ? Number(req.query.limit) : 10;
  const offset = (page - 1) * limit;
  res.json(todos.slice(offset, offset + limit));
});

app.get("/todos/:id", (req, res) => {
  const id = Number(req.params.id);
  const todo = todos.find((todo) => todo.id === id);
  if (!todo) {
    res.status(404).json({
      message: "Todo not found",
    });
  }
  res.status(200).json(todo);
});

app.post("/todos", (req, res) => {
  const newTodo = {
    id: lastId++,
    title: req.body.title,
    completed: req.body.completed || false,
    created_at: new Date().toISOString(),
    updated_at: new Date().toISOString(),
  };

  todos.push(newTodo);

  res.status(201).send();
});

app.put("/todos/:id", (req, res) => {
  const id = Number(req.params.id);
  const todo = todos.find((todo) => todo.id === id);
  if (!todo) {
    res.status(404).json({
      message: "Todo not found",
    });
  }

  todo.title = req.body.title;
  todo.completed = req.body.completed;
  todo.updated_at = new Date().toISOString();

  res.status(200).send();
});

app.delete("/todos/:id", (req, res) => {
  const id = Number(req.params.id);
  todos = todos.filter((todo) => todo.id !== id);
  res.status(204).send();
});

const server = http.createServer(app);
server.listen(8080, () => {
  console.log("Server started running on port 8080");
});
